// Grab the elements we need

const firstNameInput = document.getElementById("first-name");
const lastNameInput = document.getElementById("last-name");
const emailInput = document.getElementById("email");
const phoneInput = document.getElementById("phone");

const form = document.getElementById("form");

const errorsFirstNameDiv = document.getElementById("errors-firstname");
const errorsLastNameDiv = document.getElementById("errors-lastname");
const errorsEmailDiv = document.getElementById("errors-email");
const errorsPhoneDiv = document.getElementById("errors-phone");

// Create an object with error messages

const errorMessages = {
    tooShort: "The name is too short.",
    tooLong: "The name is too long.",
    mustContainLetters: "The name must contain letters only.",
    invalidEmail: "This is not a valid e-mail address.",
    invalidPhone: "This is not a valid phone number."
}

// Write the functions we need

/**
 * Validates the user's name.
 * 
 * @param {string} name The user's name.
 * 
 * @returns {Array} An empty array if everything's ok, otherwise an array with error messages.
 */
function validateName(name) {
    const messages = [];
    const pattern = /^[a-zA-Z ]+$/;

    if (name.length < 2) {
        messages.push(errorMessages.tooShort);
    }

    if (name.length > 20) {
        messages.push(errorMessages.tooLong);
    }

    if (!name.match(pattern)) {
        messages.push(errorMessages.mustContainLetters);
    }

    return messages;
}

function testValidateName() {
    if (validateName("Petra12").length !== 1) {
        console.error("Not working!");
    } else {
        console.log("Fine");
    }
}

/**
 * Validates the user's e-mail address.
 * 
 * @param {string} email The user's e-mail address.
 * 
 * @returns {Array} An empty array if everything's ok, otherwise an array with error messages.
 */
function validateEmail(email) {
    // The square brackets represent ONE character.
    // Inside the square brackets, you can specify all characters
    // that are allowed.
    // By using the caret symbol (^) in the square brackets, you can
    // specify the characters that are NOT allowed.
    // Here we can see [^@]+ which means:
    // Any character which is NOT an @ symbol.
    // The plus sign after it means "one or more".
    // To summarise, that means "one or more characters that are not @".

    // Read this pattern as:
    // "One or more non @ characters"
    // "Followed by ONE @ character"
    // "Followed by one or more non @ characters"
    // "Followed by a dot (\.)"
    // "Followed by one or more non @ characters"
    const pattern = /^[^@]+@[^@]+\.[^@\.]+$/;

    const messages = [];

    if (!email.match(pattern)) {
        messages.push(errorMessages.invalidEmail);
    }

    return messages;
}

/**
 * Validates the user's phone number.
 * 
 * @param {number} number The user's phone number.
 * 
 * @returns {Array} An empty array if everything's ok, otherwise an array with error messages.
 */
function validatePhoneNumber(phoneNumber) {
    // A plus character (the question mark means optional)
    // followed by one or more digits ("\d" means "digit" i.e. 0-9).
    const pattern = /^\+?\d+$/;

    const messages = [];

    if (!phoneNumber.match(pattern) || phoneNumber.length < 11) {
        messages.push(errorMessages.invalidPhone);
    }

    return messages;
}

/**
 * Creates an li containing a message.
 *
 * @param {string} string The message.
 *
 * @returns {HTMLLIElement} An li element.
 */
function messageToLi(message) {
    const li = document.createElement("li");
    li.innerHTML = message;
    return li;
}

/**
 * Creates an unordered list of error messages.
 *
 * @param {Array} array An array of error messages.
 *
 * @returns {HTMLUListElement} A ul with error messages.
 */
function arrayToUl(array) {
    const ul = document.createElement("ul");

    const lis = array.map(messageToLi);

    lis.forEach(li => ul.appendChild(li));

    return ul;
}

/**
 * Clears the error messages in the form.
 *
 * @returns void
 */
function clearErrorMessages() {
    errorsFirstNameDiv.innerHTML = "";
    errorsLastNameDiv.innerHTML = "";
    errorsEmailDiv.innerHTML = "";
    errorsPhoneDiv.innerHTML = "";
}

/**
 * Responds to the button click.
 *
 * @param {Event} event the click event.
 *
 * @returns void
 */
function handleFormSubmit(event) {
    let preventSubmit = false;

    clearErrorMessages();

    if (validateName(firstNameInput.value) !== []) {
        preventSubmit = true;
        errorsFirstNameDiv.appendChild(arrayToUl(validateName(firstNameInput.value)));
    }

    if (validateName(lastNameInput.value) !== []) {
        preventSubmit = true;
        errorsLastNameDiv.appendChild(arrayToUl(validateName(lastNameInput.value)));
    }

    if (validateEmail(emailInput.value) !== []) {
        preventSubmit = true;
        errorsEmailDiv.appendChild(arrayToUl(validateEmail(emailInput.value)));
    }

    if (validatePhoneNumber(phoneInput.value) !== []) {
        preventSubmit = true;
        errorsPhoneDiv.appendChild(arrayToUl(validatePhoneNumber(phoneInput.value)));
    }

    if (preventSubmit) {
        event.preventDefault();
    }
}

// Tie it all together

form.addEventListener("submit", handleFormSubmit);
